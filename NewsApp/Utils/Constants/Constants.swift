//
//  Constants.swift
//  NewsApp
//
//  Created by Nalinda Wickramarathna on 11/27/19.
//  Copyright © 2019 Nalinda Wickramarathna. All rights reserved.
//

import UIKit

struct Constants {
    static let newsTableViewCell = "NewsTableViewCell"
    static let showDetailsSegue = "showNewsDetailsSegue"
    static let userDefaultsNewsType = "PreferredNewsType"
    static let userDefaultsUsername = "PreferredUsername"
    static let notificationIdNewsUpdated = "NotificationIdNewsUpdated"
    
    static let actionSourceBitcoin = "Bitcoin"
    static let actionSourceApple = "Apple"
    static let actionSourceEarthquake = "Earthquake"
    static let actionSourceAnimal = "Animal"
    static let actionSourceCancel = "Cancel"
    
    static let userDefaultsNotSet = "[Not Set]"
}
