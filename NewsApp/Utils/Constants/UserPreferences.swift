//
//  UserPreferences.swift
//  NewsApp
//
//  Created by Nalinda Wickramarathna on 11/27/19.
//  Copyright © 2019 Nalinda Wickramarathna. All rights reserved.
//

import UIKit

class UserPreferences {

    static func setUserPreferences(key: String, value: String) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    static func getUserPreferences(key: String) -> String {
        return UserDefaults.standard.string(forKey: key) ?? ""
    }
}
