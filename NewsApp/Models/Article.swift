//
//  Article.swift
//  NewsApp
//
//  Created by Nalinda Wickramarathna on 11/22/19.
//  Copyright © 2019 Nalinda Wickramarathna. All rights reserved.
//

import UIKit
import SwiftyJSON

class Article: NSObject {

    var author = ""
    var title = ""
    var articleDescription = ""
    var originalURL = ""
    var imageURL = ""
    var content = ""
    var publishedAt = ""
    
    convenience init(_ article: JSON) {
        self.init()
    
        self.author = article[ArticleConstants.author].stringValue
        self.title = article[ArticleConstants.title].stringValue
        self.articleDescription = article[ArticleConstants.articleDescription].stringValue
        self.originalURL = article[ArticleConstants.originalURL].stringValue
        self.imageURL = article[ArticleConstants.imageURL].stringValue
        self.content = article[ArticleConstants.content].stringValue
        self.publishedAt = article[ArticleConstants.content].stringValue.convertToDate()
    }

}

struct ArticleConstants {
    static var articles = "articles"
    
    static var author = "author"
    static var title = "title"
    static var articleDescription = "description"
    static var originalURL = "url"
    static var imageURL = "urlToImage"
    static var publishedAt = "publishedAt"
    static var content = "content"
}

extension String {
    func convertToDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        let date = dateFormatter.date(from: self) ?? Date()
        return dateFormatter.string(from: date)
    }
}
