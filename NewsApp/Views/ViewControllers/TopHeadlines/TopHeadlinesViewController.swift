//
//  TopHeadlinesViewController.swift
//  NewsApp
//
//  Created by Nalinda Wickramarathna on 11/23/19.
//  Copyright © 2019 Nalinda Wickramarathna. All rights reserved.
//

import UIKit

class TopHeadlinesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var articles = [Article]()
    var selectedArticle = Article()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: Constants.newsTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.newsTableViewCell)
        self.tableView.tableFooterView = UIView()
        
        NewsService.fetchTopHeadlines(url: NewsService.topHeadlinesURL) { (isSuccess, articles) in
            if (isSuccess) {
                self.articles = articles
                self.tableView?.reloadData()
            }
        }
    }

}

extension TopHeadlinesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.newsTableViewCell,
                                                     for: indexPath) as! NewsTableViewCell
        cell.setData(self.articles[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.selectedArticle = self.articles[indexPath.row]
        self.performSegue(withIdentifier: Constants.showDetailsSegue, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == Constants.showDetailsSegue {
            let destinationVC = segue.destination as! NewsDetailsViewController
            destinationVC.selectedArticle = self.selectedArticle
        }
    }
}
