//
//  NewsTableViewCell.swift
//  NewsApp
//
//  Created by Nalinda Wickramarathna on 11/27/19.
//  Copyright © 2019 Nalinda Wickramarathna. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var datePublished: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(_ article: Article) {
        self.title.text = article.title
        self.datePublished.text = article.publishedAt
        
        if let url = URL(string: article.imageURL) {
            self.newsImage.kf.setImage(with: url)
        }
    }
    
}
