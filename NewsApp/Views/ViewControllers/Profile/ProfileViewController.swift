//
//  ProfileViewController.swift
//  NewsApp
//
//  Created by Nalinda Wickramarathna on 11/27/19.
//  Copyright © 2019 Nalinda Wickramarathna. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var btnPreferredNews: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.txtUsername.text = UserPreferences.getUserPreferences(key: Constants.userDefaultsUsername)
        let preferredNewsType = UserPreferences.getUserPreferences(key: Constants.userDefaultsNewsType)
        let preferredUsableNewsType = preferredNewsType != "" ? preferredNewsType : Constants.userDefaultsNotSet
        self.btnPreferredNews.setTitle(preferredUsableNewsType, for: .normal)
    }
}

extension ProfileViewController {
    
    @IBAction func newsTypeButtonTapped(_ sender: UIButton) {
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let firstAction: UIAlertAction = UIAlertAction(title: Constants.actionSourceBitcoin, style: .default) { action -> Void in
            self.savePreferences(source: Constants.actionSourceBitcoin)
        }
        let secondAction: UIAlertAction = UIAlertAction(title: Constants.actionSourceApple, style: .default) { action -> Void in
            self.savePreferences(source: Constants.actionSourceApple)
        }
        let thirdAction: UIAlertAction = UIAlertAction(title: Constants.actionSourceEarthquake, style: .default) { action -> Void in
            self.savePreferences(source: Constants.actionSourceEarthquake)
        }
        let fourthAction: UIAlertAction = UIAlertAction(title: Constants.actionSourceAnimal, style: .default) { action -> Void in
            self.savePreferences(source: Constants.actionSourceAnimal)
        }
        let cancelAction: UIAlertAction = UIAlertAction(title: Constants.actionSourceCancel, style: .cancel) { action -> Void in }
        
        actionSheetController.addAction(firstAction)
        actionSheetController.addAction(secondAction)
        actionSheetController.addAction(thirdAction)
        actionSheetController.addAction(fourthAction)
        actionSheetController.addAction(cancelAction)
        
        actionSheetController.popoverPresentationController?.sourceView = sender
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        UserPreferences.setUserPreferences(key: Constants.userDefaultsUsername, value: txtUsername.text ?? "")
        self.txtUsername.resignFirstResponder()
    }
    
    func savePreferences(source: String) {
        self.btnPreferredNews.setTitle(source, for: .normal)
        UserPreferences.setUserPreferences(key: Constants.userDefaultsNewsType, value: source)        
    }
    
}
