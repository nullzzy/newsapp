//
//  ViewController.swift
//  NewsApp
//
//  Created by Nalinda Wickramarathna on 11/22/19.
//  Copyright © 2019 Nalinda Wickramarathna. All rights reserved.
//

import UIKit
import SwiftyJSON

class HomeViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var topHeadingsContainer: UIView!
    @IBOutlet weak var myNewsContainer: UIView!
    @IBOutlet weak var profileContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func segmentValueChanged(_ sender: Any) {
        
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            topHeadingsContainer.isHidden = false
            myNewsContainer.isHidden = true
            profileContainer.isHidden = true
        break
        case 1:
            topHeadingsContainer.isHidden = true
            myNewsContainer.isHidden = false
            profileContainer.isHidden = true
            NotificationCenter.default.post(name: Notification.Name(Constants.notificationIdNewsUpdated), object: nil)
        break
        case 2:
            topHeadingsContainer.isHidden = true
            myNewsContainer.isHidden = true
            profileContainer.isHidden = false
        break
        default:
            break
        }
    }

}

