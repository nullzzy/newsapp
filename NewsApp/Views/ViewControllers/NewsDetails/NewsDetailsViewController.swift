//
//  NewsDetailsViewController.swift
//  NewsApp
//
//  Created by Nalinda Wickramarathna on 11/23/19.
//  Copyright © 2019 Nalinda Wickramarathna. All rights reserved.
//

import UIKit
import Kingfisher

class NewsDetailsViewController: UIViewController {

    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var publishedDate: UILabel!
    @IBOutlet weak var newsDescription: UITextView!
    
    var selectedArticle = Article()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.newsTitle.text = self.selectedArticle.title
        self.publishedDate.text = self.selectedArticle.publishedAt
        self.newsDescription.text = self.selectedArticle.articleDescription
        if let url = URL(string: self.selectedArticle.imageURL) {
            self.newsImageView.kf.setImage(with: url)
        }
    }

    @IBAction func viewOriginalNewsAction(_ sender: Any) {
        guard let url = URL(string: self.selectedArticle.originalURL) else { return }
        UIApplication.shared.open(url)
    }
    
}
