//
//  MyNewsViewController.swift
//  NewsApp
//
//  Created by Nalinda Wickramarathna on 11/27/19.
//  Copyright © 2019 Nalinda Wickramarathna. All rights reserved.
//

import UIKit

class MyNewsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnFilteredNews: UIButton!

    var articles = [Article]()
    var selectedArticle = Article()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: Constants.newsTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.newsTableViewCell)
        self.tableView.tableFooterView = UIView()

        NotificationCenter.default.addObserver(self, selector: #selector(self.updateNewsReceivedNotification(notification:)), name: Notification.Name(Constants.notificationIdNewsUpdated), object: nil)
    }
    
    @objc func updateNewsReceivedNotification(notification: Notification) {
        self.loadPreferredNews()
    }

    func loadPreferredNews() {
        let preferredNewsType = UserPreferences.getUserPreferences(key: Constants.userDefaultsNewsType)
        if preferredNewsType != "" {
            self.btnFilteredNews.setTitle(preferredNewsType, for: .normal)
            self.loadPreferredNews(source: preferredNewsType)
        } else {
            self.btnFilteredNews.setTitle(Constants.actionSourceApple, for: .normal)
            self.loadPreferredNews(source: Constants.actionSourceApple)
        }
    }
}

extension MyNewsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.newsTableViewCell,
                                                 for: indexPath) as! NewsTableViewCell
        cell.setData(self.articles[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.selectedArticle = self.articles[indexPath.row]
        self.performSegue(withIdentifier: Constants.showDetailsSegue, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == Constants.showDetailsSegue {
            let destinationVC = segue.destination as! NewsDetailsViewController
            destinationVC.selectedArticle = self.selectedArticle
        }
    }
}

extension MyNewsViewController {
    
    @IBAction func newsSelectionTapped(_ sender: UIButton) {
        
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let firstAction: UIAlertAction = UIAlertAction(title: Constants.actionSourceBitcoin, style: .default) { action -> Void in
            self.loadPreferredNews(source: Constants.actionSourceBitcoin)
        }
        let secondAction: UIAlertAction = UIAlertAction(title: Constants.actionSourceApple, style: .default) { action -> Void in
            self.loadPreferredNews(source: Constants.actionSourceApple)
        }
        let thirdAction: UIAlertAction = UIAlertAction(title: Constants.actionSourceEarthquake, style: .default) { action -> Void in
            self.loadPreferredNews(source: Constants.actionSourceEarthquake)
        }
        let fourthAction: UIAlertAction = UIAlertAction(title: Constants.actionSourceAnimal, style: .default) { action -> Void in
            self.loadPreferredNews(source: Constants.actionSourceAnimal)
        }
        let cancelAction: UIAlertAction = UIAlertAction(title: Constants.actionSourceCancel, style: .cancel) { action -> Void in }
        
        actionSheetController.addAction(firstAction)
        actionSheetController.addAction(secondAction)
        actionSheetController.addAction(thirdAction)
        actionSheetController.addAction(fourthAction)
        actionSheetController.addAction(cancelAction)
        
        actionSheetController.popoverPresentationController?.sourceView = sender
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func loadPreferredNews(source: String) {
        self.btnFilteredNews.setTitle("\(source) News", for: .normal)
        NewsService.fetchTopHeadlines(url: NewsService.filteredHeadlinesURL+source) { (isSuccess, articles) in
            if (isSuccess) {
                self.articles = articles
                self.tableView?.reloadData()
            }
        }
    }
    
}
