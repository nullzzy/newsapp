//
//  NewsAPI.swift
//  NewsApp
//
//  Created by Nalinda Wickramarathna on 11/22/19.
//  Copyright © 2019 Nalinda Wickramarathna. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class NewsAPI {

    func get(_ parameters: [String: Any]?, _ baseUrl: String,
             onCompletion: @escaping (_ response: DataResponse<Any>?) -> Void) {
        
        Alamofire.request(baseUrl, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                onCompletion(response)
        }
    }
}
