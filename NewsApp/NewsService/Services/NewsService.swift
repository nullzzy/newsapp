//
//  NewsService.swift
//  NewsApp
//
//  Created by Nalinda Wickramarathna on 11/22/19.
//  Copyright © 2019 Nalinda Wickramarathna. All rights reserved.
//

import UIKit
import SwiftyJSON

class NewsService {

    static let topHeadlinesURL = "https://newsapi.org/v2/top-headlines?country=us&apiKey=7c691534cc1041e49cb7bdf2c7af86e1"
    static let filteredHeadlinesURL = "https://newsapi.org/v2/top-headlines?apiKey=7c691534cc1041e49cb7bdf2c7af86e1&q="
    
    public static func fetchTopHeadlines(url: String, completion: @escaping (_ success: Bool, _ response: [Article]) -> Void) {
        
        NewsAPI().get(nil, url) { (response) in
            if let response = response {
            
                switch response.result {
                case .success(let value):
                    let json = JSON(value)

                    var articlesArray: [Article] = []
                    let articles = json[ArticleConstants.articles].arrayValue
                    for article in articles {
                        articlesArray.append(Article(article))
                    }
                    
                    completion(true, articlesArray)
                    break
                    
                case .failure(_):
                    completion(false, [])
                    break
                }
            } else {
                completion(false, [])
            }
        }
    }
}
