//
//  NewsAppTests.swift
//  NewsAppTests
//
//  Created by Nalinda Wickramarathna on 11/22/19.
//  Copyright © 2019 Nalinda Wickramarathna. All rights reserved.
//

import XCTest
@testable import NewsApp

class NewsAppTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testGetTopNewsSuccess() {
        let newsExpectation = expectation(description: "TopNewsExpectation")
        var newsResponse: [Article]?
        
        NewsService.fetchTopHeadlines(url: NewsService.topHeadlinesURL) { (isSuccess, articles) in
            if (isSuccess) {
                newsResponse = articles
                newsExpectation.fulfill()
            }
        }
        waitForExpectations(timeout: 3) { (error) in
            XCTAssertNotNil(newsResponse)
        }
    }
    
    func testGetFilteredNewsSuccess() {
        let newsExpectation = expectation(description: "FilteredNewsExpectation")
        var newsResponse: [Article]?
        let source = "Apple"
        
        NewsService.fetchTopHeadlines(url: NewsService.filteredHeadlinesURL+source) { (isSuccess, articles) in
            if (isSuccess) {
                newsResponse = articles
                newsExpectation.fulfill()
            }
        }
        waitForExpectations(timeout: 3) { (error) in
            XCTAssertNotNil(newsResponse)
        }
    }
    
    func testGetFilteredNewsFail() {
        let newsExpectation = expectation(description: "FilteredNewsFailExpectation")
        var newsResponse: [Article]?
        
        NewsService.fetchTopHeadlines(url: NewsService.filteredHeadlinesURL) { (isSuccess, articles) in
            if (isSuccess) {
                newsResponse = articles
            } else {
                newsExpectation.fulfill()
            }
        }
        waitForExpectations(timeout: 3) { (error) in
            XCTAssertNil(newsResponse)
        }
    }

}
